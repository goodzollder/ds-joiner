# DS-JOINER
Ds-joiner spark job implementation.  
This project is a part of the data source joiner application bundle.  
See [DS-BUILDER](https://bitbucket.org/goodzollder/ds-builder/src/master/) project for details.  

### Description

DS-JOINER is a simple Java application that runs on a target Spark cluster (worker).  
Based on the list of arguments received from the driver applications, it performs the following:

* Fetches data from specified elasticsearch data sources into SparkSQL datasets (in-memory)
* Performs inner or left-outer joins into another dataset
* Saves resulting (joined) dataset into elasticsearch as a new data source (index)

### Installation
The simplest way to start using the application is to run it in a bundle.  
See [DS-BUILDER](https://bitbucket.org/goodzollder/ds-builder/src/master/) project for details.    
However, the ds-joiner jar can be manually generated and copied into the shared volume for further consumption by a Spark worker.

##### Build manually

From the project's root, generate ds-joiner jar and copy it into ```ds-builder/spark-docker/volumes/jars``` shared volume.  
```bash
    ./gradlew clean build
    cp build/libs/java -jar build/libs/<jar-name> ../spark-docker/volumes/jars/
```
