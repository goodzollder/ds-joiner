import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SQLContext;
import org.apache.spark.sql.SparkSession;
import org.elasticsearch.spark.sql.api.java.JavaEsSparkSQL;

/**
 * Joins specified data sources and creates a new (joined) index.
 * Retrieves data sources to join from ES into spark data sets.
 * Performs inner and left outer joins on specified columns.
 * Indexes joined datasets into ES.
 */
public class DsJoiner {

    private static final Logger logger = LogManager.getLogger(DsJoiner.class);

    public static void main(String args[]) throws Exception {
        if (args.length < 6) {
            logger.error("Usage: DsJoiner <left-da-name> <right-ds-name> " +
                                 "<left-ds-column> <right-ds-column> " +
                                 "<joined-ds-name> <join-type: inner, leftouter>");
            System.exit(1);
        }
        String ds1 = StringUtils.lowerCase(args[0]);
        String ds2 = StringUtils.lowerCase(args[1]);
        String ds1col = StringUtils.lowerCase(args[2]);
        String ds2col = StringUtils.lowerCase(args[3]);
        String joinedDsName = StringUtils.lowerCase(args[4]);
        String joinType = StringUtils.lowerCase(args[5]);

        logger.info("Arguments: left DS [{}], right DS [{}],\nleft column [{}], right column [{}],\n" +
                            "joined DS name [{}], join type [{}]", ds1, ds2, ds1col, ds2col, joinedDsName, joinType);

        JavaSparkContext sparkContext = new JavaSparkContext();
        SQLContext sqlContext = new SQLContext(SparkSession.builder().getOrCreate());

        logger.info("Retrieving {} and {} from ES.", ds1, ds2);
        Dataset<Row> dset1 = JavaEsSparkSQL.esDF(sqlContext, ds1+ "/source");
        Dataset<Row> dset2 = JavaEsSparkSQL.esDF(sqlContext, ds2+ "/source");

        dset1.printSchema();
        dset1.show();
        dset2.printSchema();
        dset2.show();

        if ("inner".equals(joinType)) {
            logger.info("Inner joining {} and {} on {} and {}", ds1, ds2, ds1col, ds2col);
            Dataset<Row> inner = dset1.join(dset2, dset1.col(ds1col).equalTo(dset2.col(ds2col)), joinType)
                                      .orderBy(ds1col);
            inner.printSchema();
            inner.show();

            logger.info("Persisting joined datasets to ES");
            JavaEsSparkSQL.saveToEs(inner, joinedDsName + "/target");
        }
        else if ("leftouter".equals(joinType)) {
            logger.info("Left outer joining {} and {} on {} and {}", ds1, ds2, ds1col, ds2col);
            Dataset<Row> leftOuter = dset1.join(dset2, dset1.col(ds1col).equalTo(dset2.col(ds2col)), joinType)
                                          .orderBy(ds1col);
            leftOuter.printSchema();
            leftOuter.show();

            logger.info("Persisting joined datasets to ES");
            JavaEsSparkSQL.saveToEs(leftOuter, joinedDsName + "/target");
        }
        else {
            logger.error("Supported join types: inner, leftouter");
            System.exit(1);
        }

        sparkContext.close();
    }
}
